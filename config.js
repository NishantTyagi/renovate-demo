module.exports = {
    platform: 'gitlab',
    endpoint: 'https://gitlab.com/api/v4/',
    token: process.env.RENOVATE_TOKEN,

    repositories: [
        'NishantTyagi/repo-example',
    ],

    logLevel: 'info',

    requireConfig: true,
    onboarding: true,
    onboardingConfig: {
        extends: ['config:base'],
        prConcurrentLimit: 5,
    },

    enabledManagers: [
        'npm',
    ],
    packageRules: [
        {
            datasources: ["npm"],
            stabilityDays: 1
        },
    ]
}